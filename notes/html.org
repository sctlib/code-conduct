#+TITLE: HyperText Markup Language (HTML)
#+AUTHOR: i4k
#+OPTIONS: html-style:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../assets/styles.css" />

* HTML

** References

- https://developer.mozilla.org/en-US/docs/Web/HTML
- https://en.wikipedia.org/wiki/HTML

** The markup of the HTML language

HTML is a language used to create websites.

It is mainly composed of markup elements, that you can identify in an
HTML document from their surrounding =<>=; ex: =<p></p>=, =<section></section>=.

They are composed of one opening element, =<p>=, and a closing element, =</p>=.

In beetween each HTML element, lies its content, and children elements.

#+begin_src html
	<section>
		<p>Some text content</p>
		<p>Some more text, in an other html element</p>
		<p>A last bit of text, in a paragraph element, as third children on the section element</p>
	</section>
#+end_src

** Example 1

Create a [[https://duckduckgo.com/?q=how+to+create+a+new+text+file+"mac+OR+windows"][new text file]] in a folder of your computer, you can give for
example give it the name =test=, and the *extension* =.html=.

So the resulting new (empty) file, should look like =test.html= in the
file/folder explorer of your device.

Now, you can open this file in your [[https://en.wikipedia.org/wiki/Web_browser][web browser]] (Firefox/Chrome
etc.)(by right clicking on it, or double cliking it, or opening it
through the browser's menu "File > Open File").

The resulting page, should be an empty (white?) page, with no content.

If we open the =test.html= file with a [[https://en.wikipedia.org/wiki/List_of_text_editors][text editor]] (a program made to
edit text files, just like Word™, but without *bold*, *italic* and
these text formatting functions. You might already have one installed
on your device, otherwise "maybe" [[https://code.visualstudio.com][vscode]] is a good one to start with),
we can start adding content to it.

When opened, inside the =test.html= file, put the following content:

#+begin_src html
	Hello world
#+end_src

Now open the file again in your web browser, the content should be there.

This way, we've created an ultra minimal (but invalid (!), yet
working) html web page/document.

Last task for this exercise, "[[https://duckduckgo.com/?q=%22chrome+OR+firefox%22+how+to+use+the+inspetor][inspect]]" the content of the resulting
web page in your web browser, look at its [[https://www.wikihow.com/View-Source-Code][source code]].

Bonus: find the shortcut to "open the browser inspector", so you don't
have to always use the mouse to do this action.
